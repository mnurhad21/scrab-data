<?php
// curl open
$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_REFERER, 'http://www.caknun.com/jadwal');
curl_setopt($curl, CURLOPT_URL, 'http://www.caknun.com/jadwal/');

$data = curl_exec($curl);
//curl close
curl_close($curl);

// include dom
include_once 'simplehtmldom/simple_html_dom.php';

// ambil data html -> string
$html = str_get_html($data);

$output = array();
foreach ($html->find('article.jadwal') as $key => $val) {
	// ambil value ditampung divariabel
	$header = $val->find('header.entry-header', 0);
	$key    = $val->find('p.event-venue', 0);
	$date   = $val->find('p.event-date', 0)->plaintext;

	// cek data
	if(!empty($header->find('h3.entry-title'))) {
		$p  = $val->find('h2.entry-title', 0)->plaintext;
		$ps  = $val->find('h3.entry-title', 0)->plaintext;
	} else {
		$p  = $val->find('h2.entry-title', 0)->plaintext;
		$ps = '';
	}

	if(!empty($key->find('span.place'))) {
		$add  = $key->find('span.place', 0)->plaintext;
		$ads = $key->find('span.address', 0)->plaintext;
	} else {
		$add = '';
		$ads = $key->find('span.address', 0)->plaintext;
	}

	if(!empty($key->find('span.map-url'))) {
		$maps = $val->find('a',0)->href;
	} else {
		$maps = '';
	}

	//scrab data
	$output[] = array(
		'title' => $p,
		'judul_bawah' => $ps,
		'tanggal' => $date,
		'tempat' => $add,
		'address' => $ads,
		'lokasi' => $maps
	);
}

// cetak ke json
header('Content-type: application/json');
echo json_encode($output);
?>